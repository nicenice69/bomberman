package UET.Bomberman;


import UET.Bomberman.entities.*;
import UET.Bomberman.entities.Character;
import UET.Bomberman.graphics.Sprite;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javax.sound.sampled.Clip;
import java.util.ArrayList;
import java.util.List;

public class BombermanGame extends Application {
    public static final int WIDTH = 20;
    public static final int HEIGHT = 15;

    private GraphicsContext gc;
    private Canvas canvas;
    private List<Entity> entities = new ArrayList<>();
    private List<Entity> stillObjects = new ArrayList<>();
    private static Bomber bomberman;
    private GraphicsContext gc;
    private Canvas canvas;


    public static void main(String[] args) {
        Application.launch(BombermanGame.class);
    }

    @Override
    public void start(Stage stage) {
        // Tao Canvas
        canvas = new Canvas(Sprite.SCALED_SIZE * WIDTH, Sprite.SCALED_SIZE * HEIGHT);
        gc = canvas.getGraphicsContext2D();

        // Tao root container
        Group root = new Group();
        root.getChildren().add(canvas);

        // Tao scene
        Scene scene = new Scene(root);

        // Them scene vao stage
        stage.setScene(scene);
        stage.show();

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                if (running) {
                    if (upLevel && !win) {
                        WaitScene();
                        upLevel = false;
                        View.setX(-1000);
                        View.setY(-2000);
                        View.setScaleX(0.5);
                        View.setScaleY(0.5);
                    }
                    if (level != 0) {
                        render();
                        update();
                    }
                }
                if (level == 0 && !win) {
                    upLevel = true;
                }
                if (upLevel) {
                    level ++;
                    Image img;
                    if (level > LEVEL_MAX) {
                        img = new Image("Game Victory.gif");
                        win = true;
                        running = false;
                        upLevel = false;
                        View.setImage(img);
                        View.setX(96);
                        View.setY(-94);
                        View.setScaleX(1.24);
                        View.setScaleY(0.7);
                    } else {
                        img = new Image("level " + level + ".png");
                        View.setImage(img);
                        View.setX(-496);
                        View.setY(-208);
                        View.setScaleX(0.5);
                        View.setScaleY(0.5);
                    }

                    if (level <= LEVEL_MAX) {
                        _board = new Board(level);
                        _board.createMap();
                        _board.player = new Bomber(1, 1, Sprite.player_right.getFxImage());
                    }
                }
                if (!running && !win) {
                    running = true;
                    upLevel = true;
                    level = 0;
                }
            }
        };
        timer.start();

        timer.start();

        createMap();

        Entity bomberman = new Bomber(1, 1, Sprite.player_right.getFxImage());
        entities.add(bomberman);
    }

    public static List<Entity> getEntity(int x, int y) {
        List<Entity> res = new ArrayList<>();
        for (Entity e : stillObjects) {
            if (e.getXTile() == x && e.getYTile() == y) {
                res.add(e);
            }
        }
        return res;
    }


    public void createMap() {
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                Entity object;
                if (j == 0 || j == HEIGHT - 1 || i == 0 || i == WIDTH - 1) {
                    object = new Wall(i, j, Sprite.wall.getFxImage());
                }
                else {
                    object = new Grass(i, j, Sprite.grass.getFxImage());
                }
                stillObjects.add(object);
            }
        }
    }

    public void update() {
        entities.forEach(Entity::update);
    }



    public void render() {
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        stillObjects.forEach(g -> g.render(gc));
        entities.forEach(g -> g.render(gc));
    }
}
