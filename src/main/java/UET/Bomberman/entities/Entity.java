package UET.Bomberman.entities;


import UET.Bomberman.graphics.Sprite;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.canvas.GraphicsContext;
import java.awt.*;

public abstract class Entity {
    protected int x;
    protected int y;
    protected Image img;
    public ImageView imageView;

    public Entity( int xUnit, int yUnit, Image img) {
        this.x = xUnit * Sprite.SCALED_SIZE;
        this.y = yUnit * Sprite.SCALED_SIZE;
        this.img = img;
        this.imageView = new ImageView(img);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public abstract int getWidth();

    public abstract int getHeight();

    public void render(GraphicsContext gc) {
        gc.drawImage(img, x, y);
    }

}
