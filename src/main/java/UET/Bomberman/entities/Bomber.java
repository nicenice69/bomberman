package UET.Bomberman.entities;


import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import UET.Bomberman.graphics.Sprite;
import java.util.ArrayList;

public class Bomber extends Character {
    private String oldIMG = null;
    private String oldPlayerDead = null;
    private boolean isExist = true;
    private int speed = 6;
    private int width = 24;
    private int height = 32;
    private int amountBomb = 1;
    private int rangeFlame = 1;
    private int input;
    private ArrayList<Bomb> bombActive = new ArrayList<>();


    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public Bomber(int x, int y, Image img, String oldIMG) {
        super( x, y, img);
        this.oldIMG = oldIMG;
    }

    public void move(int x, int y) {
        if(x > 0) _direction = 1;
        if(x < 0) _direction = 3;
        if(y > 0) _direction = 2;
        if(y < 0) _direction = 0;

        if(canMove(0, y)) {
            setY(getY() + y);
        }

        if(canMove(x, 0)) {
            setX(getX() + x);
        }
    }

    @Override
    public boolean canMove(int x, int y) {
        for (int c = 0; c < 4; c++) {
            int xt = ((_x + x) + c % 2 * 9) / Game.TILES_SIZE;
            int yt = ((_y + y) + c / 2 * 10 - 13) / Game.TILES_SIZE;

            Entity a = _board.getEntity(xt, yt, this);

            if(!a.collide(this))
                return false;
        }

        return true;
        //return false;
    }



    public void checkAmountBombActive() {
        bombActive.removeIf(bomb -> bomb.isExploded);
    }

    public int processInput(KeyCode keyCode) {
        if (keyCode == null) return -1;
        if(keyCode == KeyCode.SPACE) return 0;
        else if(keyCode == KeyCode.LEFT || keyCode == KeyCode.A) { return 1; }
        else if(keyCode == KeyCode.RIGHT || keyCode == KeyCode.D) { return 2; }
        else if(keyCode == KeyCode.UP || keyCode == KeyCode.W) { return 3; }
        else if(keyCode == KeyCode.DOWN || keyCode == KeyCode.S) { return 4; }

        return -1;
    }

    public void processPlayerMoveUI(int req) {
        if(req == 1) {
            switch (this.oldIMG) {
                case "player_left":
                    this.img = Sprite.player_left_1.getFxImage();
                    this.oldIMG = "player_left_1";
                    break;
                case "player_left_1":
                    this.img = Sprite.player_left_2.getFxImage();
                    this.oldIMG = "player_left_2";
                    break;
                default:
                    this.img = Sprite.player_left.getFxImage();
                    this.oldIMG = "player_left";
                    break;
            }
        }
        else if(req == 2) {
            switch (this.oldIMG) {
                case "player_right":
                    this.img = Sprite.player_right_1.getFxImage();
                    this.oldIMG = "player_right_1";
                    break;
                case "player_right_1":
                    this.img = Sprite.player_right_2.getFxImage();
                    this.oldIMG = "player_right_2";
                    break;
                default:
                    this.img = Sprite.player_right.getFxImage();
                    this.oldIMG = "player_right";
                    break;
            }
        }
        else if(req == 3) {
            switch (this.oldIMG) {
                case "player_up":
                    this.img = Sprite.player_up_1.getFxImage();
                    this.oldIMG = "player_up_1";
                    break;
                case "player_up_1":
                    this.img = Sprite.player_up_2.getFxImage();
                    this.oldIMG = "player_up_2";
                    break;
                default:
                    this.img = Sprite.player_up.getFxImage();
                    this.oldIMG = "player_up";
                    break;
            }
        }
        else if(req == 4) {
            switch (this.oldIMG) {
                case "player_down":
                    this.img = Sprite.player_down_1.getFxImage();
                    this.oldIMG = "player_down_1";
                    break;
                case "player_down_1":
                    this.img = Sprite.player_down_2.getFxImage();
                    this.oldIMG = "player_down_2";
                    break;
                default:
                    this.img = Sprite.player_down.getFxImage();
                    this.oldIMG = "player_down";
                    break;
            }
        }
    }

    public boolean collide(Entity e) {

        if(e instanceof Flame){
            this.kill();
            return false;
        }
        if(e instanceof Enemy){
            this.kill();
            return true;
        }
        if( e instanceof LayeredEntity) return(e.collide(this));
        return true;
    }

    @Override
    public void render(GraphicsContext gc) {
        if (_alive)
            chooseSprite();
        else {
            _sprite = Sprite.movingSprite(Sprite.player_dead1, Sprite.player_dead2, Sprite.player_dead3, _animate, 120);
        }
        gc.drawImage(_sprite.getFxImage(), x, y);
    }

}
