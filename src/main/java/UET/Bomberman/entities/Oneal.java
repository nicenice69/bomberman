package UET.Bomberman.entities;


import UET.Bomberman.graphics.Sprite;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Random;


public class Oneal extends Enemy {
    private int horizontalSpeed = 0;
    private int verticalSpeed = 1;

    private int width = Sprite.SCALED_SIZE;
    private int height = Sprite.SCALED_SIZE;

    public Oneal(int x, int y, Image img) {
        super(x, y, img);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void updatePhysics() {
        boolean collision = canMove(this.horizontalSpeed, this.verticalSpeed);
        int changeXEnemy = this.getX() % Sprite.SCALED_SIZE;
        int changeYEnemy = this.getY() % Sprite.SCALED_SIZE;

        if (!collision) {
            this.horizontalSpeed = -1 * this.horizontalSpeed;
            this.verticalSpeed = -1 * this.verticalSpeed;
        }
        if (changeXEnemy == 0 && changeYEnemy == 0) {
            changeDirection();
        }
        this.x += horizontalSpeed;
        this.y += verticalSpeed;
    }

    private boolean canMove(int x, int y) {
        Rectangle2D rect = new Rectangle2D(x, y, Sprite.SCALED_SIZE, Sprite.SCALED_SIZE);
        Rectangle2D initRect = new Rectangle2D(this.x, this.y, Sprite.SCALED_SIZE, Sprite.SCALED_SIZE);

    }

    @Override
    protected void chooseSprite() {
        if(alive) {
            if (direction == 3) {
                img = Sprite.movingSprite(Sprite.oneal_left1, Sprite.oneal_left2, Sprite.oneal_left3, animate, 40).getFxImage();
            } else {
                img = Sprite.movingSprite(Sprite.oneal_right1, Sprite.oneal_right2, Sprite.oneal_right3, animate, 40).getFxImage();
            }
        } else {
            if(delayTime > -60) img = Sprite.oneal_dead.getFxImage();
            else if(delayTime > -90) img = Sprite.mob_dead1.getFxImage();
            else if(delayTime > -105) img = Sprite.mob_dead2.getFxImage();
            else if(delayTime > -120) img = Sprite.mob_dead3.getFxImage();
            else removed = true;
        }
    }



    private void updateSpeed(char direction, int expectedSpeed) {
        if (direction == 'U') {
            this.horizontalSpeed = 0;
            this.verticalSpeed = -expectedSpeed;
        }
        else if (direction == 'D') {
            this.horizontalSpeed = 0;
            this.verticalSpeed = expectedSpeed;
        }
        else if (direction == 'L') {
            this.horizontalSpeed = -expectedSpeed;
            this.verticalSpeed = 0;
        }
        else if (direction == 'R') {
            this.horizontalSpeed = expectedSpeed;
            this.verticalSpeed = 0;
        }
    }
}
