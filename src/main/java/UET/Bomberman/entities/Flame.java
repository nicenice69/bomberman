package UET.Bomberman.entities;


import UET.Bomberman.BombermanGame;
import UET.Bomberman.graphics.Sprite;
import UET.Bomberman.music.BombMusic;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.List;

public class Flame {
    protected double _timeToExplode = 120; //1.5 seconds
    public int _timeAfter = 25;
    protected List<Character> curChar = new ArrayList<>();
    private Flame flame;
    public boolean isExploded = false;
    public Image oldIMG = null;
    private BombMusic Music = new BombMusic();
    private static int flameLength =  1;
    private int delayTime = 120;
    protected FlameSegment[] _flameSegments = new FlameSegment[0];


    public Flame(int x, int y, Image img, int rangeFlame) {
        super(x, y, img);
        flame = new Flame(x, y, rangeFlame);
    }

    public int getFlameNumber() {

        return this.flames.length;
    }

    public double get_timeToExplode() {
        return _timeToExplode;
    }

    @Override
    public boolean canMove(double x, double y) {
        return false;
    }

    @Override
    public void render(GraphicsContext gc) {
        for (FlameSegment flameSegment : _flameSegments) {
            flameSegment.render(gc);
        }
    }

    @Override
    public void update() {
        if(delayTime > -21) {
            chooseSprite();
            --delayTime;
        }
        List<DestroyableEntity> tmp = BombermanGame.getDestroyableEntity(x / 32, y / 32);
        for (DestroyableEntity e : tmp) {
            if (e instanceof Bomb) e.Die();
            if (e instanceof MovingEntity && ((MovingEntity) e).isAlive()) {
                if((((MovingEntity) e).getY() + 3) / 32 == y / 32)
                    e.Die();
            }
        }
    }


    @Override
    public boolean canMove(double x, double y) {
        return false;
    }

}
