package UET.Bomberman.entities;

import UET.Bomberman.entities.FixedEntity;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Gift extends FixedEntity {
    protected FixedEntity item;

    public Gift(int x, int y, Image img, FixedEntity item) {
        super(x, y, img);
        this.item = item;
    }

    @Override
    public void render(GraphicsContext gc) {
        gc.drawImage(img, x, y);
    }

}
