package UET.Bomberman.entities;

import UET.Bomberman.graphics.Sprite;
import UET.Bomberman.music.BombMusic;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.List;

public class Bomb extends FixedEntity {
    protected double _timeToExplode = 120; //1.5 seconds
    public int _timeAfter = 25;
    protected List<Character> curChar = new ArrayList<>();
    private Flame flame;
    public boolean isExploded = false;
    public Image oldIMG = null;
    private BombMusic Music = new BombMusic();
    private static int flameLength =  1;
    private int delayTime = 120;


    public Bomb(int x, int y, Image img, int rangeFlame) {
        super(x, y, img);
        flame = new Flame(x, y, rangeFlame);
    }

    public int getFlameNumber() {

        return this.flames.length;
    }

    public double get_timeToExplode() {
        return _timeToExplode;
    }

    @Override
    public boolean canMove(double x, double y) {
        return false;
    }

    @Override
    public void render(GraphicsContext gc) {
        if(_exploded) {
            _sprite =  Sprite.movingSprite(Sprite.bomb_exploded, Sprite.bomb_exploded1, Sprite.bomb_exploded2, _animate, 20);
            renderFlames(gc);
        } else
            _sprite = Sprite.movingSprite(Sprite.bomb, Sprite.bomb_1, Sprite.bomb_2, _animate, 30);
        gc.drawImage(_sprite.getFxImage(), getX(), getY());
    }

    public void renderFlames(GraphicsContext gc) {
        for (Flame flame : _flames) {
            flame.render(gc);
        }
    }

    @Override
    protected void chooseSprite() {
        if(!exploded) {
            img = Sprite.movingSprite(Sprite.bomb, Sprite.bomb_1, Sprite.bomb_2, animate, 40).getFxImage();
        } else {
            if(delayTime > -4) img = Sprite.bomb_exploded.getFxImage();
            else if(delayTime > -8) img = Sprite.bomb_exploded1.getFxImage();
            else if(delayTime > -12) img = Sprite.bomb_exploded2.getFxImage();
            else if(delayTime > -16) img = Sprite.bomb_exploded1.getFxImage();
            else if(delayTime > -20) img = Sprite.bomb_exploded.getFxImage();
            else removed = true;
        }
    }


}

}
