package UET.Bomberman.entities;

import UET.Bomberman.graphics.Sprite;
import javafx.scene.image.Image;

public abstract class Enemy extends Character {
    public Enemy(int x, int y, Image img) {
        super(x, y, img);
    }

    protected abstract void changeDirection();
}
