package UET.Bomberman.entities;

import UET.Bomberman.entities.FixedEntity;
import javafx.scene.image.Image;

public class FlameItem extends Gift {
    public FlameItem(int x, int y, Image img, FixedEntity item) {
        super(x, y, img, item);
    }
}
