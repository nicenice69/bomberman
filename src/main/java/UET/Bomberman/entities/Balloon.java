package UET.Bomberman.entities;

import javafx.geometry.Rectangle2D;
import UET.Bomberman.graphics.Sprite;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Random;

public class Balloon extends Enemy {
    private int horizontalSpeed = 2;
    private int verticalSpeed = 0;

    private int width = Sprite.SCALED_SIZE;
    private int height = Sprite.SCALED_SIZE;

    public Balloon(int x, int y, Image img) {
        super(x, y, img);
    }

    public Balloon() { super(); }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void updatePhysics() {
        boolean collision = canMove(this.horizontalSpeed, this.verticalSpeed);
        int changeXEnemy = this.getX() % Sprite.SCALED_SIZE;
        int changeYEnemy = this.getY() % Sprite.SCALED_SIZE;

        if (!collision) {
            this.horizontalSpeed = -1 * this.horizontalSpeed;
            this.verticalSpeed = -1 * this.verticalSpeed;
        }
        if (changeXEnemy == 0 && changeYEnemy == 0) {
            changeDirection();
        }
        this.x += horizontalSpeed;
        this.y += verticalSpeed;
    }

    private boolean canMove(int x, int y) {
        Rectangle2D rect = new Rectangle2D(x, y, Sprite.SCALED_SIZE, Sprite.SCALED_SIZE);
        Rectangle2D initRect = new Rectangle2D(this.x, this.y, Sprite.SCALED_SIZE, Sprite.SCALED_SIZE);

    }


    public void render(GraphicsContext gc) {
        if (_alive)
            chooseSprite();
        else
            _sprite = Sprite.movingSprite(Sprite.mob_dead1, Sprite.mob_dead2, Sprite.mob_dead3, _animate, 60);
        if (_sprite != null) {
            gc.drawImage(_sprite.getFxImage(), x, y);
        }
    }

    @Override
    protected void Die() {
        if(!alive) return;
        alive = false;
    }

}
